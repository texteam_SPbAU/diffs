%10.11.15 Написана 30.11.15 MMD
\setauthor{Минин Михаил}

\begin{gather*}
y^{(n)}+p_1y^{(n-1)}+\dots+p_{n-1}(x)y'+p_ny=0\\
x^ke^{\lambda x} \ \ \lambda^n + p_1 \lambda^{n-1} \dots + p_n = 0
\end{gather*}

\begin{example}
    \begin{gather*}
        \ddot y + k \dot y+ a^2 y = 0\\
        k > 0 \ a > 0\\
        \lambda^2 + k\lambda+a^2=0\\
        \lambda = \dfrac{-k \pm \sqrt{k^2-4a^2}}{2}\\
        1) k>2a \ \lambda_1 \neq \lambda_2 < 0\\
        y(t) = C_1 e^{\lambda_1 t} + C_2 e^{\lambda_2 t}\\
        2) k=2a \ \lambda_1 = \lambda_2 =-\dfrac{k}{2}\\
        y(t) = (C_1 t + C_2) e^{-\dfrac{kt}{2}}\\
        3) k<2a \\
        \lambda = \dfrac{-k}{2} \pm \dfrac{i \sqrt{4a^2 - k^2}}{2}\\
        y(t) = (C_1 cos(\dfrac{\sqrt{4a^2 - k^2}}{2}t) + C_2 sin(\dfrac{\sqrt{4a^2 - k^2}}{2}t)) e^{-\dfrac{kt}{2}}
    \end{gather*}
\end{example}


\section{Неоднородные скалярные уравнения \checkmark}

\begin{gather*}
y^{(n)}+p_1(x)y^{(n-1)}+\dots+p_{n-1}y'+p_n(x)y=q(x) \ \ (1)\\
\end{gather*}
Общее решение неоднородного уравнения = общее решение однородного + частное решение неоднородного
\subsection*{Метод вариации произвольных постоянных \checkmark}

$\sqsupset \varphi_1 , \dots , \varphi_n$ - фундаментальная система решений однородного уравнения.

$y=C_1(x) \varphi_1(x) + \dots + C_n(x)\varphi_n(x)$

$y^\prime = C_1 \varphi_1^\prime + \dots + C_n\varphi_n^\prime + \underbrace{C_1^\prime \varphi_1 + \dots + C_n^\prime\varphi_n}_0$

Скажем что вторая половина равна нулю (просто берем и говорим, без всяких предпосылок и доказательств корректности) Точно также берем все n производных.

\begin{gather*}
y^\prime = C_1 \varphi_1^\prime + \dots + C_n\varphi_n^\prime + \underbrace{C_1^\prime \varphi_1 + \dots + C_n^\prime\varphi_n}_0\\
y^{\prime\prime} = C_1 \varphi_1^{\prime \prime} + \dots + C_n\varphi_n^{\prime \prime} + \underbrace{C_1^\prime \varphi_1^\prime + \dots + C_n^\prime\varphi_n^\prime}_0\\
\vdots\\
y^{(n-1)} = C_1 \varphi_1^{(n-1)}+ \dots + C_n\varphi_n^{(n-1)}+ \underbrace{C_1^\prime \varphi_1^{(n-2)} + \dots + C_n^\prime\varphi_n^{(n-2)}}_0\\
y^{(n)} = C_1 \varphi_1^{(n)} + \dots + C_n\varphi_n^{(n)} + C_1^\prime \varphi_1^{(n-1)} + \dots + C_n^\prime\varphi_n^{(n-1)}
\end{gather*}
Вводим функцию $\mathscr{L}$:

$\mathscr{L}(y) = y^{(n)} + p_1 y^{(n-1)} + \dots + p_n y$, где коэффициенты из исходного уравнения (1)

$\mathscr{L} (y) = C_1 \mathscr{L} (\varphi_1) + C_2 \mathscr{L} (\varphi_2) + \dots + C_n \mathscr{L} (\varphi_n) +  C_1^\prime \varphi_1^{(n-1)} + \dots + C_n^\prime\varphi_n^{(n-1)}$

$\mathscr{L} (\varphi_i) = 0$ Так как $\varphi$ - решение однородного уравнения. Составляем систему из наших условий систему:
$$
\left\{\begin{aligned}
&C_1^\prime \varphi_1 + \dots + C_n^\prime\varphi_n = 0\\
&C_1^\prime \varphi_1^\prime + \dots + C_n^\prime\varphi_n^\prime = 0\\
&\vdots\\
&C_1^\prime \varphi_1^{(n-2)} + \dots + C_n^\prime\varphi_n^{(n-2)} = 0\\
&C_1^\prime \varphi_1^{(n-1)} + \dots + C_n^\prime\varphi_n^{(n-1)} = q(x)
\end{aligned}
\right.
$$
Если функции C удовлетворяют системе, то $y$ решение уравнения. Вронскиан не 0: $W(x) \neq 0$, т.к. линейно независимые.

$C^\prime_j = \dfrac{q(x)}{W} W_{nj}(x)$, где $W_{nj}$ - алгебраическое дополнение

\begin{theorem}
$\sqsupset \varphi_1 , \dots , \varphi_n$ - фундаментальные решения однородного уравнения.

$y^{(n)} + p_1 y^{(n-1)} + \dots + p_n y = 0$ Тогда у неоднородного уравнения имеется частное решение:

$y(x) = \sum\limits_{j=1}^n \varphi_j(x) \int\limits_{x_0}^x \frac{W_{nj}(t)}{W(t)}q(t)dt$,  где $W_{nj}$ - алгебраическое дополнение

Общее решение:
$y(x) = \sum_{j=1}^n \varphi_j(x) \left(D_j + \int\limits_{x_0}^x \frac{W_{nj}(t)}{W(t)}q(t)dt\right)$, где $D_j$ - константы
\end{theorem}

Алгоритм:

Находим $\varphi_1, \dots , \varphi_n$, далее $y(x) = C_1(x) \varphi_1(x) + \dots + C_n(x)\varphi_n(x)$, затем находим $C_i$

\section{Краевые задачи для уравнений второго порядка \checkmark}

Здесь живем в $n = 2$.

\subsection*{Уравнение струны \checkmark}

\begin{tikzpicture}[domain=0:6.3, scale=1]
%Сетка
    \draw[very thin,color=gray] (-1.1,-1.1) grid (5.9,1.9);
%Оси
    \draw[->] (-1.2,0) -- (7.2,0) node[right] {$x$};
    \draw[->] (0,-1.2) -- (0,2.2) node[above] {$v(t,x)$};
    \draw[thick, thick, color=red] plot[id=x] function{sin(x)};

%Точка (0, 0) и (6.3, 0)
    \filldraw (0, 0)circle(1.2pt);
    \filldraw (6.3, 0)circle(1.2pt);

    \filldraw (1.8, 0.97384763087)circle(1.2pt);
    \filldraw (2.1, 0.86320936664)circle(1.2pt);

    \draw[very thick] (1.8, -0.05) -- (1.8, 0.05) node[below = 6pt, left = 2pt]
    {$x_0$};
    \draw[very thick] (2.1, -0.05) -- (2.1, 0.05) node[below = 5pt, right= 1pt]
    {$x_0+dx$};

\end{tikzpicture}

Из физики:  $m\vec a = \vec{F_0} - \vec{F_1}$, где $|F_0|=|F_1| = T$ - натяжение струны.

$-T\sin \alpha_0 + T \sin \alpha_1 = m \dfrac{\partial^2 u}{\partial t^2}$

$\alpha$ - малы, тогда $\sin \alpha_0 \approx \tan \alpha_0 = \dfrac{\partial u}{\partial x} (x_0)$.
Равенство из того, что производная равна тангенсу угла наклона.

$\sin \alpha_1 - \sin \alpha_0 = \dfrac{\partial u}{\partial x} (x_0 + dx) - \dfrac{\partial u}{\partial x} (x_0) = \dfrac{\partial^2 u}{\partial t^2}(x_0)dx$

Домножаем это выражение на $T$, и вводим $\rho$ - линейная плотность: $m = \rho dx$, и получаем:

$\begin{aligned}
\dfrac{\partial^2 u(t, x)}{\partial t^2} = V^2\dfrac{\partial^2 u}{\partial x^2}\text{, где } V= \sqrt\frac{T}{\rho}
\end{aligned}$

Решать такое мы еще не умеем. Решение:

$u(t, x) = e^{i\omega t} y(x) $

Условие на то, что концы зафиксированы:

$u(t, 0) = u(t, l) = 0$, $\left.\frac{\partial u}{\partial t}\right|_{t=0} = 0$

$$
\left\{\begin{aligned}
& -y^{\prime \prime} = \left(\frac{w}{v}\right)^2 y(x)\\
& y(0) = y(l) = 0
\end{aligned}
\right.
$$

Из первого уравнения понятно, что решение $\sin$ или $\cos$

$y(x) = \sin {(\dfrac{n\pi x}{l} )}$, $\dfrac{\omega}{V} = \dfrac{n\pi }{l}$, где $n \in \mathbb{N}$

$u(t,x) = \cos{(\frac{n\pi x}{l}t)} \sin {(\frac{n\pi x}{l})}$

Здесь был разговор об обертонах и прочее.

\subsection*{Краевые задачи для уравнений второго порядка \checkmark}

$$
\left\{\begin{aligned}
& y^{\prime \prime}(x) + p_1(x) y^\prime (x) + p_2 (x) y(x) = f(x)\\
& y(a) = 0, \  y(b) = 0 \ \ x\in [a,b]
\end{aligned}
\right. (1)
$$

\begin{warning}
    Пусть на крае не ноль $y(a) = A, \ y(b)=B$

    \begin{gather*}
    \sqsupset z \in C[a,b] \ z(a) = A, z(b) = B\\
    \tilde y = y - z\\
    \left\{\begin{aligned}
    & \tilde y^{\prime \prime}(x) + p_1(x) \tilde y^\prime (x) + p_2 (x) \tilde y(x) = \tilde f(x)\\
    &\tilde  y(a) = 0, \ \tilde  y(b) = 0 \ \ x\in [a,b]
    \end{aligned}
    \right.
    \end{gather*}

\end{warning}
\begin{warning}
    Если дано краевое условие не только для $y$ на границах интервала:

    $y(a) = 0 \ y^\prime (a)= 0$ (Или равно А)

    $\alpha_0 y(a) + \alpha_1 y^\prime(a) = 0$ или A.  $\alpha_0^2 + \alpha_1^2 \neq 0$

    Важно то, что в краевых точках a и b дано хотя бы по одному условию.
\end{warning} 